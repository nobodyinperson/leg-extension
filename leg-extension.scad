// clang-format off
include <utils/utils.scad>;
use <threads-scad/threads.scad>;
// clang-format on

/* [👁 Display] */
// 🦶 Show the leg
show_leg = true;
// Length of the leg
leg_length = 100; // [10:0.1:600]
// ⚔ Slice it only in preview mode to show the inside
slice_in_preview = true;
// render some operations to get rid of glitches
render_some = false;
// How far to position the parts from each other
part_clearance = 10; // [0:0.1:300]

/* [🏗 Construction] */
// 📏 space around the foot (e.g. 3D printer accuracy - in doubt use MORE!!)
clearance = 0.2; // [0:0.01:1]
// 📏 how much to elevate the foot
foundation_height = 30; // [0:0.1:200]
// 📏 how far to enclose the foot
rim_height = 50; // [5:0.1:300]
// 📏 thickness of the rim
rim_thickness = 3.5; // [1:0.1:30]
// 🫂 Ensure the foot is completely enclosed
base_ensure_foot_enclosed = true;
// Check this to do a hull over the hole base (useful for stands)
base_overall_hull = false;

/* [🦶 Foot Hole ⚫]  */
// Shape of the foot to heighten
foot_shape_type = "⬛ square"; // ["⬛ square", "⚫ cylindrical","📁 file"]
// 📁 File for the foot if hole_type is "📁 file"
foot_shape_file = "";
// ◯ Edge smoothing radius for hole shape
foot_shape_edge_radius = 5; // [0:0.1:20]
// 🔺 Whether to chamfer the foot edges instead of rounding it
foot_shape_chamfer = false;
// Use !=0 for a regular polygon with type="⚫ cylindrical"
foot_shape_edges = 0;
// 📏 [width,depth] to scale foot shape to. Set to [0,0] to skip resizing for imported files
foot_shape_size = [ 50, 50 ]; //  [0:0.1:300]

// 🔄 Rotate the foot shape
foot_shape_angle = 0; // [-180:1:180]

/* [🦶 Bottom Shape]  */
// Shape of the base standing on the ground (without the screwable height adjuster)
base_shape_type = "⚫ cylindrical"; // ["⬛ square", "⚫ cylindrical","📁 file"]
// 📁 File for the foot if hole_type is "📁 file"
base_shape_file = "";
// ◯ Edge smoothing radius for the base bottom
base_shape_edge_radius = 5; // [0:0.1:20]
// 🔺 Whether to chamfer the base edges instead of rounding it
base_shape_chamfer = false;
// Use !=0 for a regular polygon with type="⚫ cylindrical"
base_shape_edges = 0;
// 📏 [width,depth] to scale base shape to. Set to [0,0] to skip resizing for imported files
base_shape_size = [ 50, 50 ]; //  [0:0.1:300]
// 🔄 Rotate the base shape
base_shape_angle = 0; // [-180:1:180]

/* [🔩 Screw Adjuster] */
// 🔩 whether to enable the screwable height adjuster
with_height_adjuster = true;
// 📏 Height of the adjuster
adjuster_height = 10; // [1:0.1:100]

/* [🔩 Screw Adjuster Thread] */
adjuster_thread_diameter = 20;                                                // [1:0.1:100]
adjuster_thread_pitch = 2;                                                    // [0.1:0.1:10]
adjuster_thread_tooth_angle = 45;                                             // [20:1:80]
adjuster_thread_length_ = max([ 0, foundation_height - 2 * rim_thickness ]);  // [1:0.1:100]
adjuster_thread_hole_length_ = max([ 0, foundation_height - rim_thickness ]); // [1:0.1:100]
// space around the screw. Use MORE if in doubt!
adjuster_thread_hole_tolerance = 0.5; // [0.1:0.01:10]
// Use 1 for flat top of the thread hole
adjuster_thread_hole_tip_min_fract = 0.6; // [0:0.01:1]

/* [🔩 Screw Adjuster Top Shape] */
// Shape of the adjuster top
adjuster_top_shape_type = "⚫ cylindrical"; // ["⬛ square", "⚫ cylindrical","📁 file"]
// 📁 File for the adjuster top if adjuster_top_shape_type is "📁 file"
adjuster_top_shape_file = "";
// ◯ Edge smoothing radius for the adjuster top
adjuster_top_shape_edge_radius = 5; // [0:0.1:20]
// 🔺 Whether to chamfer the adjuster top nstead of rounding it
adjuster_top_shape_chamfer = false;
// Use !=0 for a regular polygon with type="⚫ cylindrical"
adjuster_top_shape_edges = 0;
// 📏 [width,depth] to scale adjuster top to. Set to [0,0] to skip resizing for imported files
adjuster_top_shape_size = [ 50, 50 ]; //  [0:0.1:300]
// 🔄 Rotate the adjuster top shape
adjuster_top_shape_angle = 0; // [-180:1:180]

/* [🔩 Screw Adjuster Bottom Shape] */
// Shape of the adjuster bottom
adjuster_bottom_shape_type = "⚫ cylindrical"; // ["⬛ square", "⚫ cylindrical","📁 file"]
// 📁 File for the adjuster bottom if adjuster_bottom_shape_type is "📁 file"
adjuster_bottom_shape_file = "";
// ◯ Edge smoothing radius for the adjuster bottom
adjuster_bottom_shape_edge_radius = 5; // [0:0.1:20]
// 🔺 Whether to chamfer the adjuster bottom nstead of rounding it
adjuster_bottom_shape_chamfer = false;
// Use !=0 for a regular polygon with type="⚫ cylindrical"
adjuster_bottom_shape_edges = 0;
// 📏 [width,depth] to scale adjuster bottom to. Set to [0,0] to skip resizing for imported files
adjuster_bottom_shape_size = [ 50, 50 ]; //  [0:0.1:300]
// 🔄 Rotate the adjuster bottom shape
adjuster_bottom_shape_angle = 0; // [-180:1:180]

// Precision
$fs = $preview ? 3 : 0.5;
$fa = $preview ? 3 : 0.5;
epsilon = 0.1 * 1; // to hide it from the Customizer

module
make_shape(type = "⬛ square",
           file = undef,
           edge_radius = 0,
           edges = 0,
           chamfer = false,
           size = [ 100, 100 ],
           angle = 0,
           position = [ 0, 0 ])
{
  rotate([ 0, 0, angle ]) offset_chain([ -edge_radius, edge_radius ], chamfer = chamfer)
    resize_if(size != [ 0, 0 ], size) if (type == "⬛ square") square(size, center = true);
  else if (type == "⚫ cylindrical") if (edges > 0) circle(d = max(size), $fn = edges);
  else circle(d = max(size));
  else if (type == "📁 file") import(file, center = true);
  else assert(false, "Invalid shape");
}

module
foot_shape()
{
  make_shape(type = foot_shape_type,
             file = foot_shape_file,
             edge_radius = foot_shape_edge_radius,
             edges = foot_shape_edges,
             chamfer = foot_shape_chamfer,
             size = foot_shape_size,
             angle = foot_shape_angle);
}

module
foot_hole_shape()
{
  offset(clearance) foot_shape();
}

module
foot_hole_mask()
{
  linear_extrude(rim_height + epsilon) foot_hole_shape();
}

module
base_top_shape()
{
  offset(rim_thickness) foot_hole_shape();
}

module
base_bottom_shape()
{
  make_shape(type = base_shape_type,
             file = base_shape_file,
             edge_radius = base_shape_edge_radius,
             edges = base_shape_edges,
             chamfer = base_shape_chamfer,
             size = base_shape_size,
             angle = base_shape_angle);
}

module
adjuster_top_shape()
{
  make_shape(type = adjuster_top_shape_type,
             file = adjuster_top_shape_file,
             edge_radius = adjuster_top_shape_edge_radius,
             chamfer = adjuster_top_shape_chamfer,
             edges = adjuster_top_shape_edges,
             size = adjuster_top_shape_size,
             angle = adjuster_top_shape_angle);
}

module
adjuster_bottom_shape()
{
  make_shape(type = adjuster_bottom_shape_type,
             file = adjuster_bottom_shape_file,
             edge_radius = adjuster_bottom_shape_edge_radius,
             chamfer = adjuster_bottom_shape_chamfer,
             edges = adjuster_bottom_shape_edges,
             size = adjuster_bottom_shape_size,
             angle = adjuster_bottom_shape_angle);
}

module
adjuster_thread()
{
  ScrewThread(outer_diam = adjuster_thread_diameter,
              height = adjuster_thread_length_,
              pitch = adjuster_thread_pitch,
              tooth_angle = adjuster_thread_tooth_angle,
              tip_height = min([ 3, adjuster_thread_length_ * 0.9 ]),
              tip_min_fract = 0.7);
}

module
adjuster_thread_hole(height = adjuster_thread_hole_length_, enable = true)
{
  if (enable)
    ScrewHole(outer_diam = adjuster_thread_diameter,
              height = height,
              pitch = adjuster_thread_pitch,
              tooth_angle = adjuster_thread_tooth_angle,
              tolerance = adjuster_thread_hole_tolerance,
              tip_height = min([ rim_thickness, adjuster_thread_hole_length_ * 0.9 ]),
              tip_min_fract = adjuster_thread_hole_tip_min_fract) children();
  else
    children();
}

module
base()
{
  difference()
  {
    //   ____                        ____           _
    //  / ___|___  _ __  _   _      |  _ \ __ _ ___| |_ ___
    // | |   / _ \| '_ \| | | |_____| |_) / _` / __| __/ _ \
    // | |__| (_) | |_) | |_| |_____|  __/ (_| \__ \ ||  __/
    //  \____\___/| .__/ \__, |     |_|   \__,_|___/\__\___|
    //            |_|    |___/
    //     _    _                      _
    //    / \  | | __ _ _ __ _ __ ___ | |
    //   / _ \ | |/ _` | '__| '_ ` _ \| |
    //  / ___ \| | (_| | |  | | | | | |_|
    // /_/   \_\_|\__,_|_|  |_| |_| |_(_)
    //
    // OpenSCAD's limitation of doing lazy-children passing...
    if (base_overall_hull)
      hull()
      {
        linear_extrude(epsilon) base_bottom_shape();
        if (base_ensure_foot_enclosed)
          translate([ 0, 0, foundation_height ]) translate([ 0, 0, -epsilon ]) linear_extrude(epsilon) base_top_shape();
        translate([ 0, 0, foundation_height ]) translate([ 0, 0, rim_height ]) translate([ 0, 0, -epsilon ])
          linear_extrude(epsilon) base_top_shape();
      }
    else
      chained_hull()
      {
        linear_extrude(epsilon) base_bottom_shape();
        if (base_ensure_foot_enclosed)
          translate([ 0, 0, foundation_height ]) translate([ 0, 0, -epsilon ]) linear_extrude(epsilon) base_top_shape();
        translate([ 0, 0, foundation_height ]) translate([ 0, 0, rim_height ]) translate([ 0, 0, -epsilon ])
          linear_extrude(epsilon) base_top_shape();
      }

    translate([ 0, 0, foundation_height ]) foot_hole_mask();
  }
}

module
adjuster_body()
{
  hull()
  {
    linear_extrude(epsilon) adjuster_bottom_shape();
    translate([ 0, 0, adjuster_height ]) translate([ 0, 0, -epsilon ]) linear_extrude(epsilon) adjuster_top_shape();
  }
}

module
adjuster()
{
  translate([ 0, 0, -adjuster_height ]) adjuster_body();
  adjuster_thread();
}

if (show_leg) {
  debug() translate([ 0, 0, foundation_height + part_clearance ]) linear_extrude(leg_length) foot_shape();
}
intersection()
{
  union()
  {
    adjuster_thread_hole(enable = with_height_adjuster) base();
    if (with_height_adjuster) {
      translate([ 0, 0, -part_clearance ]) rotate([ 0, 0, -part_clearance / adjuster_thread_pitch * 360 ]) adjuster();
    }
  }
  if (slice_in_preview && $preview) {
    translate([ 0, 0, -adjuster_height - part_clearance - epsilon / 2 ]) cube([
      max([ foot_shape_size.x, base_shape_size.x, adjuster_top_shape_size.x, adjuster_bottom_shape_size.x ]) / 2 +
        rim_thickness,
      max([ foot_shape_size.y, base_shape_size.y, adjuster_top_shape_size.y, adjuster_bottom_shape_size.y ]) / 2 +
        rim_thickness,
      foundation_height + max([ leg_length + part_clearance, rim_height ]) + adjuster_height + part_clearance +
      epsilon
    ]);
  }
}
